// /backend/movie.js
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// this will be our data base's data structure 
const MovieSchema = new Schema(
  {
    _id: String,
    title: String,
    year: String,
    type : String,
    poster : String
  },
  { 
      timestamps: true 
   }
);

// export the new Schema so we could modify it using Node.js
module.exports = mongoose.model("Movie", MovieSchema);