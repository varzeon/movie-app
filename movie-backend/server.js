const mongoose = require("mongoose");
const express = require("express");
const bodyParser = require("body-parser");
const logger = require("morgan");
const Movie = require("./movie");
const cors = require('cors')

const API_PORT = 3001;
const app = express();
const router = express.Router();

// this is our MongoDB database
const dbRoute = 'mongodb://localhost:27017/movie-app';

// connects our back end code with the database
mongoose.connect(
  dbRoute,
  { useNewUrlParser: true }
);

let db = mongoose.connection;

db.once("open", () => console.log("connected to the database"));

// checks if connection with the database is successful
db.on("error", console.error.bind(console, "MongoDB connection error:"));

// (optional) only made for logging and
// bodyParser, parses the request body to be a readable json format
app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(logger("dev"));


// this is our get method
// this method fetches all available movies in our database
router.get("/getAllMovies", (req, res) => {
  var page = 1;
  if (req.query.page != undefined) {
    page = req.query.page;
  }
  Movie.find((err, data) => {
    if (err) {
        return res.json({ success: false, error: err });
    } else {
        var movies = data;
        if (data.length > 10) {
          movies = data.slice( (page - 1) * 10, ((page - 1) * 10) + 10);
        }        
        var pageMovies = movies.map(movie => ({ imdbID: movie._id,  Title: movie.title, Year: movie.year, Type : movie.type, Poster : movie.poster}));
        return res.json({ success: true, Search:  pageMovies, totalResults: data.length});
    }
  });
});

// this is our update method
// this method overwrites existing movie in our database
router.post("/updateMovie", (req, res) => {
    const { Title, Year, imdbID, Type, Poster } = req.body;
    let movie = new Movie();
    movie._id = imdbID;
    movie.title = Title;
    movie.year = Year;
    movie.type = Type;
    movie.poster = Poster;
    Movie.findOneAndUpdate({_id : imdbID}, movie, err => {
    if (err) return res.json({ success: false, error: err });
    return res.json({ success: true });
  });
});

// this is our delete method
// this method removes existing movie in our database
router.delete("/deleteMovie", (req, res) => {
  const { imdbID } = req.body;
  Movie.findOneAndDelete({_id: imdbID}, err => {
    if (err) return res.send(err);
    return res.json({ success: true });
  });
});

// this is our create movie method
// this method adds new movie in our database
router.post("/addMovie", (req, res) => {  
  console.log('Add Movie: ' + req);
  const { Title, Year, imdbID, Type, Poster } = req.body;
  console.log('Add Movie: ' + Title + Year + imdbID);
  if ((!imdbID && imdbID !== 0) || !Title) {
    return res.json({
      success: false,
      error: "INVALID INPUTS"
    });
  }
  let movie = new Movie();
  movie._id = imdbID;
  movie.title = Title;
  movie.year = Year;
  movie.type = Type;
  movie.poster = Poster;
  movie.save(err => {
    if (err) return res.json({ success: false, error: err });
    console.log('Add Movie SUCCESS');
    return res.json({ success: true, message: 'Movie saved successfully!' });
  });
});

// append /api for our http requests
app.use("/api/v1", router);

// launch our backend into a port
app.listen(API_PORT, () => console.log(`LISTENING ON PORT ${API_PORT}`));