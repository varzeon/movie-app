import React, {Component} from 'react';
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import InputBase from '@material-ui/core/InputBase';
import { fade } from '@material-ui/core/styles/colorManipulator';
import { withStyles } from '@material-ui/core/styles';
import MenuIcon from '@material-ui/icons/Menu';
import SearchIcon from '@material-ui/icons/Search';
import Button from '@material-ui/core/Button';

const styles = theme => ({
  root: {
    width: '100%',
  },
  grow: {
    flexGrow: 1,
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },
  title: {
    display: 'none',
    [theme.breakpoints.up('sm')]: {
      display: 'block',
    },
  },
  search: {
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.white, 0.25),
    },
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing.unit,
      width: 'auto',
    },
  },
  searchIcon: {
    width: theme.spacing.unit * 9,
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputRoot: {
    color: 'inherit',
    width: '100%',
  },
  inputInput: {
    paddingTop: theme.spacing.unit,
    paddingRight: theme.spacing.unit,
    paddingBottom: theme.spacing.unit,
    paddingLeft: theme.spacing.unit * 10,
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      width: 120,
      '&:focus': {
        width: 500,
      },
    },
  },
  button: {
    margin: theme.spacing.unit,
  },
});

class CInput extends Component {
  constructor(props) {
    super(props);
  }
  
  render(){
     return <InputBase onChange={this.props.changeHandler}
      placeholder="Search movie …   "
      classes={{
        root: this.props.inputRoot,  
        input: this.props.inputInput,
      }}
   />
  }
}

class SearchAppBar extends Component {
  constructor(props) {
    super(props);
    this.state = {searchStr: ''};
    this.changeHandler = this.changeHandler.bind(this);
  }

  changeHandler(event){
    this.setState({searchStr: event.target.value}, () => {
      console.log('Value Changed!: ' + this.state.searchStr);
    });
  }

  render() {
    return <div className={this.props.classes.root}>
      <AppBar position="static">
        <Toolbar>
          <IconButton className={this.props.classes.menuButton} color="inherit" aria-label="Open drawer">
            <MenuIcon />
          </IconButton>
          <Typography className={this.props.classes.title} variant="h6" color="inherit" noWrap>
            Movie Search App
          </Typography>
          <div className={this.props.classes.grow} />
          <div className={this.props.classes.search}>
            <div className={this.props.classes.searchIcon}>
              <SearchIcon />
            </div>
            <CInput
              changeHandler={this.changeHandler}
              inputRoot={this.props.classes.inputRoot} 
              inputInput={this.props.classes.inputInput}
            /> 
          </div>
          <Button variant="contained" autofocus color="secondary" onClick={() => this.props.searchHandler(this.state.searchStr)} className={this.props.classes.button}>
            Search
          </Button>
        </Toolbar>
      </AppBar>
    </div>
  }  
}

SearchAppBar.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(SearchAppBar);
