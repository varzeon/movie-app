import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import MovieCard from './MovieCard';

const styles = {
    /* Base */
    body: {
        background: '#F3F3F3',
        padding: 20
    },
  
    /* Container */
    container: {
        display: 'flex',
        flexDirection: 'row',
        flexWrap: 'wrap',
        width: '90%',
        maxWidth: 960,
        margin: '0 auto'
    },
  
    /* Column */
    column: {
        flexBasis: 'percentage(1/3)',
        width: 'percentage(1/3)',
        padding: '0 10px',
        boxSizing: 'border-box'
    },

    category: {
        display: 'inline-block',
        // background: #212121;
        padding: '8px 10px',
        margin: '0 0 10px',
        color: '#FFF',
        fontSize: '0.75rem',
        fontWeight: 600,
        letterSpacing: '0.075rem',
        textTransform: 'uppercase'
      }
};

/*
  CardContainer
  <CardContainer />
*/
class CardContainer extends Component {
    constructor(props){
        super(props);
    }
    render() {
      return <div className={this.props.classes.app}>
          <div className={this.props.classes.container}>
            {this.props.movies.map((item, index) => {
                return (
                  <div className="column">
                      <MovieCard movieIndex={index} details={item} showMoreHandler={this.props.showMoreHandler} saveHandler={this.props.saveHandler}/>
                  </div>          
                )
              })}
          </div>
        </div>
    }
}

CardContainer.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(CardContainer);
  