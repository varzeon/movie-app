import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

const styles = {
  card: {
    minWidth: 400,
    maxWidth: 400,
    height: 150,
    textAlign: 'left',
    margin: '10px'
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
  image: {
      maxWidth: 100,
      height: 125
  },
  child: {
    float: 'left',
    marginLeft: '10px'
  }
};

class MovieCard extends Component {
    constructor(props) {
        super(props);
    } 
  

  render() {
      var titleWithDots = this.props.details.Title;
      if (titleWithDots.length >= 18) {
        titleWithDots = titleWithDots.substring(0, 18);
        titleWithDots += '...'
      }
      var validPoster = this.props.details.Poster;
      if (validPoster === 'N/A') {
          validPoster = 'http://localhost:3000/noposter.jpg';
      }
      
    return <Card className={this.props.classes.card}>
        <CardContent>
            <div className={this.props.classes.child}>
                <img className={this.props.classes.image} src={validPoster} alt={this.props.details.Title}/>
            </div>
            <div className={this.props.classes.child}>
                <Typography className={this.props.classes.title} color="textSecondary" gutterBottom>
                    Year of Release: {this.props.details.Year}
                </Typography>
                <Typography variant="h5" component="h2">
                    {titleWithDots}
                </Typography>
                <Typography className={this.props.classes.pos} color="textSecondary">
                    IMDB ID: {this.props.details.imdbID}
                </Typography>
                <Typography component="p">
                    {this.props.details.type} 
                </Typography>
                <Button size="small" onClick={() => this.props.showMoreHandler(this.props.movieIndex)}>Show More</Button> <Button size="small" onClick={() => this.props.saveHandler(this.props.movieIndex)} >Save</Button>
            </div>        
      </CardContent>
      <CardActions>
        
      </CardActions>
    </Card>
  }
}

MovieCard.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(MovieCard);