import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import MuiDialogActions from '@material-ui/core/DialogActions';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Typography from '@material-ui/core/Typography';

const DialogTitle = withStyles(theme => ({
  root: {
    borderBottom: `1px solid ${theme.palette.divider}`,
    margin: 0,
    padding: theme.spacing.unit * 2,
  },
  closeButton: {
    position: 'absolute',
    right: theme.spacing.unit,
    top: theme.spacing.unit,
    color: theme.palette.grey[500],
  },
}))(props => {
  const { children, classes, onClose } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root}>
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton aria-label="Close" className={classes.closeButton} onClick={onClose}>
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const DialogContent = withStyles(theme => ({
  root: {
    margin: 0,
    padding: theme.spacing.unit * 2,
  },
}))(MuiDialogContent);

const DialogActions = withStyles(theme => ({
  root: {
    borderTop: `1px solid ${theme.palette.divider}`,
    margin: 0,
    padding: theme.spacing.unit,
  },
}))(MuiDialogActions);

class MovieDialog extends React.Component {  

  render() {
    return (
      <div>
        <Dialog
          onClose={this.props.handleDialogClose}
          aria-labelledby="customized-dialog-title"
          open={this.props.open}
        >
          <DialogTitle id="customized-dialog-title" onClose={this.props.handleDialogClose}>
            {this.props.movieData.Title} (IMDB ID: {this.props.movieData.imdbID})
          </DialogTitle>
          <DialogContent>
            <Typography gutterBottom>
                Year: {this.props.movieData.Year} / Release: {this.props.movieData.Released}
            </Typography>
            <Typography gutterBottom>
                Runtime: {this.props.movieData.Runtime}
            </Typography>
            <Typography gutterBottom>
                Genre: {this.props.movieData.Genre} / Type: {this.props.movieData.Type}
            </Typography>
            <Typography gutterBottom>
                Director: {this.props.movieData.Director} / Writer: {this.props.movieData.Writer}
            </Typography>
            <Typography gutterBottom>
                Actors: {this.props.movieData.Actors}
            </Typography>
            <Typography gutterBottom>
                Plot: {this.props.movieData.Plot}
            </Typography>
            <Typography gutterBottom>
                Language: {this.props.movieData.Language} / Country: {this.props.movieData.Country}
            </Typography>
            <Typography gutterBottom>
                imdbRating: {this.props.movieData.imdbRating} ({this.props.movieData.imdbVotes})
            </Typography>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.props.handleDialogClose} color="primary">
               OK
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

export default MovieDialog;