import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import CardContainer from './Components/CardContainer'
import SearchAppBar from './Components/SearchAppBar'
import LabelBottomNavigation from './Components/LabelBottomNavigation';
import CustomSnackbar from './Components/CustomSnackBar'
import InfiniteScroll from 'react-infinite-scroller/dist/InfiniteScroll';
import MovieDialog from './Components/MovieDialog';

const OMDB_URI = 'http://www.omdbapi.com/';
const OMDB_API_KEY = 'bd5eef22';

// BACKEND SERVER DETAILS
const MOVIE_SERVER_HOST = 'http://localhost:3001';
const MOVIE_API_PATH = '/api/v1';
const SAVE_MOVIE_URL = MOVIE_SERVER_HOST + MOVIE_API_PATH + '/addMovie';
const GET_MOVIES_URL = MOVIE_SERVER_HOST + MOVIE_API_PATH + '/getAllMovies';

var getOmdbAPI = (searchStr, page) => OMDB_URI + '?s=' + searchStr + '&apiKey=' + OMDB_API_KEY + '&page=' + page + '&';
var getMovieAPI = (imdbID) => OMDB_URI + '?i=' + imdbID + '&apiKey=' + OMDB_API_KEY + '&';

class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      movies:[], 
      currentPage:1, 
      totalPages:1, 
      hasMorePages: false, 
      searchStr: '', 
      dialogOpen: false, 
      movieData: {},
      snackOpen: false, 
      snackVarient: 'success', 
      snackMessage: '',
      apiType: 'OMDB'
    };
    this.searchHandler = this.searchHandler.bind(this);
    this.scrollHandler = this.scrollHandler.bind(this);
    this.saveHandler = this.saveHandler.bind(this);
    this.handleSnackClose = this.handleSnackClose.bind(this);
    this.handleDialogClose = this.handleDialogClose.bind(this);
    this.favoritesHandler = this.favoritesHandler.bind(this);
    this.loadMovies = this.loadMovies.bind(this);
    this.showMoreHandler = this.showMoreHandler.bind(this);
  }

  loadMovies(page) {
    if (this.state.currentPage <= this.state.totalPages) {
      var api= '';
      if (this.state.apiType === 'OMDB') {
        api = getOmdbAPI(this.state.searchStr, this.state.currentPage);
      } else {
        api = GET_MOVIES_URL + '?page=' + this.state.currentPage;
      }
      
      fetch(api)
      .then(response => response.json())
      .then(data => this.setState((state, props) => ({ movies: state.movies.concat(data.Search), currentPage: state.currentPage+1 , totalPages: Math.ceil(data.totalResults / 10) })))
      .then(() => {
        console.log('Movies: ' + JSON.stringify(this.state.movies));
        if (this.state.currentPage >= this.state.totalPages) {
          this.setState({hasMorePages : false});
        }      
      });
    }  else {
      console.log('No more pages to show... ' + this.state.currentPage + ' ' + this.state.totalPages)
    }  
  }

  isBottom() {
    //return el.getBoundingClientRect().bottom <= window.innerHeight;
    
    var isBottom = false;
    const windowHeight = "innerHeight" in window ? window.innerHeight : document.documentElement.offsetHeight;
    const body = document.body;
    const html = document.documentElement;
    const docHeight = Math.max(body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight);
    const windowBottom = windowHeight + window.pageYOffset;
    if (windowBottom >= docHeight) {
        isBottom = true;
    } 
    return isBottom;
  }
  
  componentDidMount() {
    document.addEventListener('scroll', this.scrollHandler);
  }
  
  componentWillUnmount() {
    document.removeEventListener('scroll', this.scrollHandler);
  }

  searchHandler(searchStr) {
    console.log('Movie to be Searched: ' + searchStr);
    this.setState({movies: [], searchStr: searchStr, currentPage: 1, totalPages: 1, hasMorePages: true, apiType: 'OMDB'});
    /*
    let api = getOmdbAPI(searchStr, this.state.currentPage);
    fetch(api)
    .then(response => response.json())
    .then(data => this.setState({ movies: data.Search, currentPage:1, totalPages: Math.ceil(data.totalResults / 10) }))
    .then(() => console.log('Movies: ' + JSON.stringify(this.state.movies)));
    */
  }

  favoritesHandler() {
    this.setState({movies: [], currentPage: 1, totalPages: 1, hasMorePages : true, apiType: 'FAVORITES'});
    /*
    let api = GET_MOVIES_URL;
    fetch(api)
    .then(response => response.json())
    .then(data => this.setState({ movies: data.Search, currentPage:1, totalPages: Math.ceil(data.totalResults / 10) }))
    .then(() => console.log('Movies: ' + JSON.stringify(this.state.movies)));
    */
  }

  handleSnackClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }

    this.setState({ snackOpen: false });
  };

  handleDialogClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    this.setState({ dialogOpen: false });
  };

  showMoreHandler(movieIndex) {
    var movie = this.state.movies[movieIndex];
    let api = getMovieAPI(movie.imdbID);
    fetch(api)
    .then(response => response.json())
    .then((data) => {
      console.log('Movie Data: ' + JSON.stringify(data));
      this.setState({ dialogOpen: true, movieData: data });
    });
  }

  saveHandler(movieIndex) {
    console.log('Movie Index to be Saved: ' + movieIndex);
    var movie = this.state.movies[movieIndex];
    console.log('Movie to be Saved: ' + JSON.stringify(movie));
    let api = SAVE_MOVIE_URL;
    fetch(api, {method: 'post',headers: {
      'Content-Type': 'application/json'
    }, body: JSON.stringify(movie)})
    .then(response => response.json())
    .then(data => {
      console.log('Response: ' + JSON.stringify(data))
      this.setState({ snackOpen: true, snackVarient: data.success ? 'success' : 'error', snackMessage: data.success ? data.message : data.error });
    });
    
  }

  scrollHandler = (e) => {  
    //const wrappedElement = document.getElementById('ScrollContainer');
    if (this.isBottom()) { 
      this.loadMovies();
      /*
        if (this.state.currentPage + 1 <= this.state.totalPages) {
          let api = getOmdbAPI(this.state.searchStr, this.state.currentPage + 1);
          fetch(api)
          .then(response => response.json())
          .then(data => this.setState((state, props) => ({ movies: state.movies.concat(data.Search), currentPage: state.currentPage+1 })))
          .then(() => console.log('Movies: ' + JSON.stringify(this.state.movies)));
        }
        */
    }
  }

  render() {
    const loader = <div className="loader">Loading ...</div>;
    return (
      <div className="App">        
        <header >
          <SearchAppBar searchHandler={this.searchHandler}/>          
        </header>
        <CustomSnackbar varient={this.state.snackVarient} open={this.state.snackOpen} handleSnackClose={this.handleSnackClose} message={this.state.snackMessage}/>
        <MovieDialog open={this.state.dialogOpen} handleDialogClose={this.handleDialogClose} movieData={this.state.movieData}/>
        <InfiniteScroll
          pageStart={this.state.currentPage}
          loadMore={this.loadMovies}
          hasMore={this.state.hasMorePages}
          loader={loader}>
          <div id="ScrollContainer" className="App-loader" onScroll={this.scrollHandler}>
            <CardContainer movies={this.state.movies} saveHandler={this.saveHandler} showMoreHandler={this.showMoreHandler}/>
          </div>
        </InfiniteScroll>
        <footer>
          <LabelBottomNavigation favoritesHandler={this.favoritesHandler}/>
        </footer>        
      </div>
    );
  }
}

export default App;
